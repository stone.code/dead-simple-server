// Copyright 2014 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

/*
Dead Simple Server (DSS) is s an extremely simple web server. It can
serve the contents of a specified folder tree on a specified port.  It
does nothing else.

The web server uses the defaults from the Go standard library.  These defaults
are not suitable for public-facing servers.  In particular, the server does
not have any connection timeouts set, nor does it use HTTPS.
*/
package main
