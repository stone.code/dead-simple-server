// Copyright 2014 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
)

var (
	address = flag.String("address", ":80", "Address for the HTTP server.")
	path    = flag.String("path", "./", "Root folder to serve.")
	version = flag.Bool("version", false, "Print version information and exit.")

	versionInformation = "development"
)

// LogHandler wraps a http.Handler, and adds logging to every request.
type LogHandler struct {
	Next http.Handler
}

func (l *LogHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Print("Request from ", r.RemoteAddr)
	l.Next.ServeHTTP(w, r)
}

func main() {
	flag.Parse()
	if *version {
		fmt.Fprintf(os.Stdout, "dead simple server (%s)\n", versionInformation)
		os.Exit(0)
	}

	log.Print("Serving at address ", *address)
	handler := &LogHandler{http.FileServer(http.Dir(*path))}
	err := http.ListenAndServe(*address, handler)
	if err != nil {
		log.Fatal(err)
	}
}
