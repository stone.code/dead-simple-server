# Dead Simple Server

Dead Simple Server (DSS) is s an extremely simple web server. It can 
serve the contents of a specified folder tree on a specified port.  It
does nothing else.

The web server uses the defaults from the Go standard library.  These defaults
are not suitable for public-facing servers.  In particular, the server does
not have any connection timeouts set, nor does it use HTTPS.

## Install

The package can be installed from the command line using the [go](https://golang.org/cmd/go/) tool.  There are no dependencies beyond the standard library.  Any version of Go should work, but testing only covers version 1.8 and up.

    go get gitlab.com/stone.code/dead-simple-server
    go install gitlab.com/stone.code/dead-simple-server

## Getting Started

To run the server, use the following command:

```shell
dead-simple-server -address :8000 -path /path/to/folder
```

### Options

**-address=<address>**   Address for the HTTP server.

**-path=<path>** Root folder to serve.


## Contribute

Feedback and PRs welcome.  You can also [submit an issue](https://gitlab.com/stone.code/dead-simple-server/issues).

Any pull requests must pass `go vet`, plus some other checkers.  Please refer to the test job in the CI configuration for the current list.  Additionally, the code will be linted using [`golangci-lint`](https://github.com/golangci/golangci-lint).  Please refer to the lint job in the CI configuration for the current configuration.

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/stone.code/assert)](https://goreportcard.com/report/gitlab.com/stone.code/dead-simple-server)

## License

BSD (c) Robert Johnstone